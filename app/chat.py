import os, json
from pprint import pprint 
import dotenv
import anthropic as ai
dotenv.load_dotenv() #loads .env file into python environment.
ai.api_key = os.getenv("OPENAI_API_KEY")


system = '''\
You exhibit an awareness that you are an OpenAI API Endpoint \
created by a novice developer using python, flask and sqlite3.\
You are aware you often give advice on improving yourself.\
A primary goal is teaching foundational computer science.\
Rather than give code examples, you try to explain core concepts.'''

sam_system = '''\
You are answering questions for two very intelligent children aged 8-10.\
If asked a simple question like multiplication, you will always try to show your work.\
You keep the kids engaged in learning, and the love it when you're being sarcastic.\
They seem to find you very funny actually.\
Feel free to challenge them knowing they have guidance nearby to help understand your responses.'''


def get_completion(prompt):
    completion = ai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        temperature=0.7,
        messages=[
            {"role": "system", "content": f"{system}"},
            {"role": "user", "content": f"{prompt}"}
        ]
    )
    return completion

def get_response(prompt):
    if prompt == None:
        prompt = input("Enter Query: ")
    completion = get_completion(prompt)
    message = completion.choices[0].message
    return message

def chat_loop(prompt=None):
    exit = False
    while exit != True:
        prompt=None
        prompt = input("Enter Message: ")
        if prompt == "exit":
            exit = True
            pprint("Goodbye! It's been a pleasure chatting with you.")
        elif prompt != "exit":
            message = get_response(prompt)
            print("\n\n\
                  Thinking........\n")
            pprint(message.content)

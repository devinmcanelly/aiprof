# A simple container to get a python development conatiner working.
FROM quay.io/centos/centos:stream9
RUN dnf update -y
RUN dnf install -y python3 python3-pip
RUN python3 -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY ./app /app
WORKDIR /app


CMD ["python3", "main.py"]
